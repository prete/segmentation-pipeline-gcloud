#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2020 Tong LI <tongli.bioinfo@gmail.com>
#
# Distributed under terms of the BSD-3 license.

"""
Extract channel(s) from a tif
"""

from dask_image.imread import imread
from dask import array

import numpy as np
import argparse

import tifffile as tf


def main():

    argparser = argparse.ArgumentParser()

    argparser.add_argument(
        "-file_in",
        type=argparse.FileType("r"),
        required=True,
        help="The file to be split, should have single-z plane",
    )
    argparser.add_argument(
        "-ch_indexes",
        type=int,
        nargs="+",
        required=True,
        help="The indexes (0-based) of channels to be extracted",
    )
    argparser.add_argument(
        "-out_dir", type=str, required=False, default=".", help="Output directory"
    )
    argparser.add_argument("-prefix", type=str, required=False)
    argparser.add_argument(
        "-chunk_size", type=int, nargs="+", required=False, default=(1000, 1000)
    )
    argparser.add_argument("-format", type=str, required=False)

    args = argparser.parse_args()

    full_img = imread(args.file_in.name)

    chs_to_process = full_img[args.ch_indexes]

    assert chs_to_process.ndim == 3

    suffix = "_".join([str(i) for i in args.ch_indexes])

    if args.format == "npy":
        np.save(
            "%s/%s_%s"
            % (
                args.out_dir,
                args.prefix,
                suffix,
                np.expand_dims(chs_to_process, (0, 1)),
            )
        )
    # elif args.format == "zarr":
    # rechunked_ch = array.rechunk(,
    # chunks=args.chunk_size).compute()
    # rechunked_ch.to_zarr("%s/%s.zr" %(args.out_dir, ch_ind))
    else:
        tf.imsave(
            "%s/%s_%s.tif" % (args.out_dir, args.prefix, suffix),
            np.array(chs_to_process),
            bigtiff=True,
        )


if __name__ == "__main__":
    main()
