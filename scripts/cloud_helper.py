#! /usr/bin/env python
# -*- coding: utf-8 -*-

# requires pyyaml
# pip install --upgrade pyyaml

import os
import glob
import argparse
import subprocess
import yaml

BUCKET="segmentation-input-images"

def main(args):
    
    if not os.path.exists(args.params_file):
        raise FileNotFoundError(f"'{args.params_file}' does not exist")

    with open(args.params_file) as pf:
        params = yaml.safe_load(pf)
    
    if 'project' not in params:
        raise Exception("Missing or empty 'project' key in parameters file.")

    # check input path
    if not 'img_path' in params:
        raise Exception(f"Missing 'img_path' parameters")
    if not glob.glob(params['img_path']):
        raise Exception(f"Invalid 'img_path' parameter. Bad file or permissons to access the file.")

    # add cloud paths
    if not params['img_path'].startswith('gs://') and not 'cloud_img_path' in params:
        print("Adding 'cloud_in_dir' key to parameters file...")
        params["cloud_in_dir"] = f"gs://{BUCKET}/projects/{params['project']}/input"
        print("Adding 'cloud_img_path' key to params file...")
        params["cloud_img_path"] = f"{params['cloud_in_dir']}/{os.path.basename(params['img_path'])}"
    if not params['out_dir'].startswith('gs://') and not 'cloud_out_dir' in params:
        print("Adding 'cloud_out_dir' key to parameters file...")
        params["cloud_out_dir"] = f"gs://{BUCKET}/projects/{params['project']}/output/"
    with open(args.params_file, "w") as pf:
        yaml.dump(params, pf)

    if args.get_profile:
        list_of_files = filter(os.path.isfile, glob.glob(params['img_path']))
        max_file_size = os.stat(max(list_of_files, key=lambda x: os.stat(x).st_size)).st_size
        if max_file_size < 5*1024*1024*1024: #<5 GB
            print("small")
        elif max_file_size < 75*1024*1024*1024: #<75 GB
            print("medium")
        else:
            print("large")
        exit(0)

    # copy files to the cloud
    if args.upload_input:
        img_path = params['img_path']
        cloud_in_dir = params['cloud_in_dir']
        result = subprocess.run([f'gcloud alpha storage cp -r "{img_path}" "{cloud_in_dir}"'], shell=True, check=True)
        exit(0)

    # download files from the
    if args.download_output:
        cloud_out_dir = params['cloud_out_dir']
        out_dir = params['out_dir']
        result = subprocess.run([f'gcloud alpha storage cp -r "{cloud_out_dir}" "{out_dir}"'], shell=True, check=True)
        exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("params_file", help="Parameters file in YAML format. Same one that the nextlfow pipeline will use as `-params-file` option.")
    parser.add_argument("--upload-input", action="store_true", help="Upload images from `img_path` to the cloud bucket.")
    parser.add_argument("--download-output", action="store_true", help="Download pipeline results from the cloud bucket to `out_dir`.")
    parser.add_argument("--get-profile", action="store_true", help="Get profile (small/medium/large) based of the largest file on `img_path`.")
    args = parser.parse_args()
    main(args)
