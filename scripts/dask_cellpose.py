#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2021 Tong LI <tongli.bioinfo@protonmail.com>
#
# Distributed under terms of the BSD-3 license.

"""
Run CellPose with map_blocks from dask
"""
import argparse
from dask_image import imread
import dask.array as da
from cellpose import models
import numpy as np
import tifffile as tf
from dask.distributed import Client, progress

import numpy as np
from scipy import ndimage as ndimg
from time import time


def run_cellpose_2d(
    imgs_2D,
    channels=[0, 0],
    flow_threshold=None,
    cell_size=None,
    use_GPU=True,
    model_type="cyto",
):
    # DEFINE CELLPOSE MODEL
    # model_type='cyto' or model_type='nuclei'
    model = models.Cellpose(gpu=use_GPU, model_type=model_type)

    # define CHANNELS to run segementation on
    # grayscale=0, R=1, G=2, B=3
    # channels = [cytoplasm, nucleus]
    # if NUCLEUS channel does not exist, set the second channel to 0
    # channels = [0,0]
    # IF ALL YOUR IMAGES ARE THE SAME TYPE, you can give a list with 2 elements
    # channels = [0,0] # IF YOU HAVE GRAYSCALE
    # channels = [2,3] # IF YOU HAVE G=cytoplasm and B=nucleus
    # channels = [2,1] # IF YOU HAVE G=cytoplasm and R=nucleus

    # if diameter is set to None, the size of the cells is estimated on a per image basis
    # you can set the average cell `diameter` in pixels yourself (recommended)
    # diameter can be a list or a single number for all images

    masks, flows, styles, diams = model.eval(
        [imgs_2D],
        diameter=cell_size,
        flow_threshold=flow_threshold,
        channels=[channels],
    )
    return masks[0], flows[0]


def run_cellpose_3d(
    imgs_3D,
    channels=[0, 0],
    flow_threshold=None,
    cell_size=None,
    use_GPU=True,
    model_type="cyto",
):
    # model_type='cyto' or model_type='nuclei'
    model = models.Cellpose(gpu=use_GPU, model_type=model_type)

    # define CHANNELS to run segementation on
    # grayscale=0, R=1, G=2, B=3
    # channels = [cytoplasm, nucleus]
    # if NUCLEUS channel does not exist, set the second channel to 0
    # channels = [0,0]

    # if diameter is set to None, the size of the cells is estimated on a per image basis
    # you can set the average cell `diameter` in pixels yourself (recommended)
    # diameter can be a list or a single number for all images

    mask, flow, style, diam = model.eval(
        imgs_3D,
        diameter=cell_size,
        flow_threshold=flow_threshold,
        channels=channels,
        do_3D=True,
    )
    return mask


def main(args):
    target_ch = imread.imread(args.ch_img).squeeze()

    if target_ch.ndim > 3:
        rechunked = target_ch.rechunk((-1, args.tilesize, args.tilesize, -1))
        rechunked = da.array(np.expand_dims(rechunked, 1))
        seg = rechunked.map_blocks(
            run_cellpose_3d,
            flow_threshold=args.flow_threshold,
            channels=args.channels,
            cell_size=args.cell_size,
            model_type=args.model_type,
            drop_axis=1,
            dtype=np.uint32,
        )
    else:
        # rechunked = target_ch.rechunk((-1, args.tilesize, args.tilesize))
        # seg = rechunked.map_blocks(
            # run_cellpose_2d,
            # flow_threshold=args.flow_threshold,
            # channels=args.channels,
            # cell_size=args.cell_size,
            # model_type=args.model_type,
            # dtype=np.int32,
        # )

        seg, flow = run_cellpose_2d(np.array(target_ch).astype(np.uint32),
                flow_threshold=args.flow_threshold,
                channels=args.channels,
                cell_size=args.cell_size,
                model_type=args.model_type
            )

    # start = time()
    # water, core, msk = flow2msk(flow[0], None, 1.0, 20, 100)
    # print('flow to mask cost:', time()-start)

    img_shape_str= "_".join(np.array(target_ch.shape).astype(str))
    tf.imwrite("%s_%s_label.tif" % (args.prefix, img_shape_str), seg.astype(np.uint32), dtype=np.uint32)
    # tf.imwrite("%s_%s_flow.tif" % (args.prefix, img_shape_str), seg)
    # tf.imwrite("%s_%s_custom_mask.tif" % (args.prefix, img_shape_str), msk)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-ch_img", type=str, required=True)
    parser.add_argument("-prefix", type=str, required=True)
    parser.add_argument("-cell_size", type=int, required=True)
    parser.add_argument("-flow_threshold", type=float, required=True)
    parser.add_argument("-tilesize", type=str, default="auto")
    parser.add_argument("-model_type", type=str, default="nuclei")
    parser.add_argument("-channels", type=int, nargs="+", default=0)

    args = parser.parse_args()

    # client = Client(
        # threads_per_worker=4, n_workers=1, processes=False, memory_limit="250GB"
    # )
    main(args)
