#!/usr/bin/env nextflow

params.dapi_ch = 0
params.cyto_ch = ''
params.object_diameter = 70
params.tile_size = 5000
params.flow_threshold = 0.0
params.channels = "0 0"
params.model_type = "nuclei"
params.magnification = 40
params.expand = 70

log.info """\

===========================================================
 I M A G I N G | S E G M E N T A T I O N | P I P E L I N E
===========================================================

 Parameters
 ===========================================================
 project         : ${params.project}
 img_path        : ${params.img_path}
 └─cloud_path    : ${params.cloud_img_path}
 out_dir         : ${params.out_dir}
 └─cloud_path    : ${params.cloud_out_dir}
 dapi_ch         : ${params.dapi_ch}
 cyto_ch         : ${params.cyto_ch}
 object_diameter : ${params.object_diameter}
 tile_size       : ${params.tile_size}
 flow_threshold  : ${params.flow_threshold}
 expand          : ${params.expand}
 magnification   : ${params.magnification}
 ===========================================================
 """


process extract_channels_from_input_image {
    container "eu.gcr.io/imaging-gpu-eval/extract_channels:latest"
    storeDir params.cloud_out_dir
    label 'basic_cpu'
    // gcp preemptive error strategy
    errorStrategy = { task.exitStatus==14 ? 'retry' : 'terminate' }
    
    input:
    file img from Channel.fromPath(params.cloud_img_path)
 
    output:
    tuple val(stem), file("${stem}_${params.dapi_ch}.tif") into target_chs, target_chs_for_filtering

    script:
    stem = img.baseName
    """
    extract_channels -file_in ${img} -ch_indexes ${params.dapi_ch} $params.cyto_ch -prefix "${stem}"
    """
}


process cellpose_cell_segmentation {
    container "eu.gcr.io/imaging-gpu-eval/cellpose:0.6.5-pytorch1.9-cuda11.1"
    storeDir params.cloud_out_dir
    label 'gpu'
    // gcp preemptive error strategy
    errorStrategy = { task.exitStatus==14 ? 'retry' : 'terminate' }

    input:
    tuple val(stem), file(ch_img) from target_chs

    output:
    tuple val(stem), file("${stem}*label.tif") into labels, labels_to_filter

    script:
    """
    dask_cellpose -ch_img ${ch_img} $params.cyto_ch -prefix "$stem" -cell_size ${params.object_diameter} -flow_threshold ${params.flow_threshold} -channels ${params.channels} -model_type ${params.model_type}
    """
}


process expand_labels {
    container "eu.gcr.io/imaging-gpu-eval/expand_labels:latest"
    storeDir params.cloud_out_dir
    label 'basic_cpu'
    // gcp preemptive error strategy
    errorStrategy = { task.exitStatus==14 ? 'retry' : 'terminate' }

    input:
    tuple val(stem), file(label) from labels

    output:
    tuple val(stem), file("${stem}*_label_expanded.tif") into expanded_nuc_labels, labels_for_filtering

    script:
    """
    expand_labels -label ${label} -prefix ${stem} -distance ${params.expand}
    """
}

process ilastik_cell_filtering {
    echo true
    container "eu.gcr.io/imaging-gpu-eval/ilastik:latest"
    storeDir params.cloud_out_dir + "/" + params.magnification + "x_cell_mask"
    label 'intense_cpu'
    // gcp preemptive error strategy
    errorStrategy = { task.exitStatus==14 ? 'retry' : 'terminate' }

    input:
    tuple val(stem), file(raw_img), file(mask) from target_chs_for_filtering.join(expanded_nuc_labels)

    output:
    tuple val(stem), file("$stem*table.csv") into cell_roi_descriptions
    tuple val(stem), file("$stem*Object Predictions.tif") into selective_masks, selective_masks_for_intensity_quant

    script:
    """
    #LAZYFLOW_THREADS=10 LAZYFLOW_TOTAL_RAM_MB=60000
    bash /ilastik-1.3.3-Linux/run_ilastik.sh --headless \
        --project=/model/project.ilp \
        --readonly \
        --table_filename="./${stem}_object_features.csv" \
        --export_source="Blockwise Object Predictions" \
        --output_format="tif" \
        --raw_data=${raw_img} \
        --segmentation_image=${mask}
    """
}

process filter_labels {
    echo true
    container "eu.gcr.io/imaging-gpu-eval/filter_labels:latest"
    storeDir params.cloud_out_dir
    label 'basic_cpu'
    // gcp preemptive error strategy
    errorStrategy = { task.exitStatus==14 ? 'retry' : 'terminate' }

    input:
    tuple val(stem), file(label), file(mask), file(roi) from labels_for_filtering.join(selective_masks).join(cell_roi_descriptions)

    output:
    tuple val(stem), file("$stem*filtered_labels.tif")
    tuple val(stem), file("$stem*filtered_rois.tsv")

    script:
    """
    filter_labels -label ${label} -prefix ${stem} -mask ${mask} -rois ${roi}
    """
}
