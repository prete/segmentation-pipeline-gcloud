# Segmentation Pipeline on Google LifeSciences

## Setup

1. [Install nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation)
2. Enable Google LifeSciences API. From Google Cloud Console search for Google Cloud Life Sciences API >  click enable
3. Create Service Account. From Google Cloud Console search for IAM & Admin > Service Accounts > Create service account “nextflow” and grant the following permissions:
  - `Cloud Life Sciences Workflows Runner`
  - `Service Account User`
  - `Service Usage Consumer`
  - `Storage Object Admin`
4. Grant the `Storage Admin` and `Storage Object Admin` permissions to Google LifeScience Service Account, it's the one with the name like "service-xxx@gcp-sa-lifesciences.iam.gserviceaccount.com"
5. Create your service account key file following [this steps](https://cloud.google.com/docs/authentication/getting-started) and use service accounts with the Cloud SDK setting you env `GOOGLE_APPLICATION_CREDENTIALS` to that file.
6. [Install gsutils](https://cloud.google.com/storage/docs/gsutil_install)
7. [Activate the srvice account on the terminal](https://cloud.google.com/sdk/gcloud/reference/auth/activate-service-account) so you don't have to login each time you use gcloud/gsutil.

## Containers

Each step of the pipeline uses a different container to keep things modular.
- **process extract_channels_from_input_image**: eu.gcr.io/imaging-gpu-eval/extract_channels:latest
- **process cellpose_cell_segmentation**: eu.gcr.io/imaging-gpu-eval/cellpose:latest
- **process expand_labels**: eu.gcr.io/imaging-gpu-eval/expand_labels:latest
- **process ilastik_cell_filtering**: eu.gcr.io/imaging-gpu-eval/ilastik:latest
- **process filter_labels**: eu.gcr.io/imaging-gpu-eval/filter_labels:latest

All containers can be built using the `buld_containers.sh` script.

Dockerfiles are located inside the `containers/` folder and any extra Python source code for each particular container is picked up from the `scripts/` folder.

## Running the pipeline

### Method 1: automatic upload/download

First create a `params.yml` file with your desired values based on your input data. For example, this are the default values:
```yaml
project: <project name, ie. 'MY_PRJ'>
img_path: "/some/place/in/image.tiff"
out_dir: "/some/place/out/"
dapi_ch: 0
cyto_ch: ''
object_diameter: 70
tile_size: 5000
flow_threshold: 0.0
channels: "0 0"
model_type: "nuclei"
magnification: 40
expand: 70
```

Triggger the upload/runpipeline/download process by submitting the job:
```bash
bsub -q imaging \
     -o segmentation-%J-output.log \
     -e segmentation-%J-error.log \
     -n2 \
     -M16000 \
     -R"select[mem>16000] rusage[mem=16000]" \
     /nfs/cellgeni/imaging-cloud/segmentation-pipeline-gcloud/launch /path/to/params.yaml
```

### Method 2: manual upload/download
First copy the required input data to the cloud using 
```bash
gsutil cp /local/project/image.tff gs://cellgen-segmentation-pipeline/<project>/intput/
```

Secondly, create a `params.yml` file with your desired values based on your input data. You'll need to add `project`, `cloud_img_path` and `cloud_out_dir` to it for the cloud pipeline to be able to use the right bucket locations.
For example, this are the default values plust the cloud_* parameters:
```yaml
project: <project name>
img_path: "/local/project/image.tff"
cloud_img_path: gs://segmentation-input-images/projects/<project>/input/image.tiff"
cloud_out_dir: gs://segmentation-input-images/projects/<projcet>/output/
dapi_ch: 0
cyto_ch: ''
object_diameter: 70
tile_size: 5000
flow_threshold: 0.0
channels: "0 0"
model_type: "nuclei"
magnification: 40
expand: 70
```

Based on the input size of your data you'll also need to pick between `small` (<5 GB), `medium` (>5 GB, < 35 GB) and `large` (>35 GB) profiles. These profiles will change the resource requirements (CPU/RAM/GPU) for the VMs at each step. This is because larger input files require more RAM.

Now run
```bash
nextflow run main.nf -params-file params.yaml -profile gls,medium -with-report
```
(this example uses the `medium` profile but as mentioned you can also use `small` or `large`)

Finally, copy the genearted output data from the cloud (`cloud_out_dir` parameter) to your local storage using 
```bash
gsutil cp -r gs://cellgen-segmentation-pipeline/projects/<project>/output/ /local/project/segmentation_output/
```